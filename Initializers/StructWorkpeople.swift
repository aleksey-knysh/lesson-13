//
//  StructWorkpeople.swift
//  Initializers
//
//  Created by Aleksey Knysh on 2/22/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import Foundation

// MARK: - struct Worcpeople
struct Worcpeople {
    var name: String
    var surname: String
    // default initializer
    var presenceAtPost: Bool?
    var workPass = true
    
    // with set internal parameters
    init() {
        name = "Игнат"
        surname = "Рабочий"
    }
    
    // with external parameters
    init(name: String, surname: String) {
        self.name = name
        self.surname = surname
    }
    
    // the helper initializer invokes another
    init(name: String) {
        self.init(name: name, surname: "медведев")
    }
}
