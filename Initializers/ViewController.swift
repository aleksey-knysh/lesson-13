//
//  ViewController.swift
//  Initializers
//
//  Created by Aleksey Knysh on 2/22/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var worker: Workers!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        worker = Workers(name: "Жора", surname: "Жирный", presenceAtPost: true)
        let secondClassInit = Workers()
        let thirdClassInit = Workers(name: "Василий", surname: "Арбайтавич")
        let firstStructInit = Worcpeople()
        let secondStructInit = Worcpeople(name: "Анджей", surname: "Польский")
        let firstStudentInit = Student()
        let secondStudentInit = Student(nameStudent: "Аня", surnameStudent: "Обычная")
        
        let arrayInit = [secondClassInit, thirdClassInit, firstStructInit, secondStructInit, firstStudentInit, secondStudentInit] as [Any]
        
        arrayInit.forEach { print($0) }
        
        print(secondClassInit.name)
    }
}
