//
//  ClassWorkers.swift
//  Initializers
//
//  Created by Aleksey Knysh on 2/22/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import Foundation

class Workers {
    var name: String
    var surname: String
    var presenceAtPost: Bool?
    var workPass = true
    
    // default initializer
    init(name: String = "Жора", surname: String = "Жирный", presenceAtPost: Bool? = true) {
        self.surname = surname
        self.name = name
    }
    
    // with set internal parameters
    init() {
        name = "Борис"
        surname = "Корягин"
    }
    
    // with external parameters
    init(name: String, surname: String) {
        self.name = name
        self.surname = surname
    }
    
    // the helper initializer invokes another
    convenience init(name: String) {
        self.init(name: name, surname: "дружбан")
    }
    
    deinit {
        print("\nОбъект \(name) уничтожен")
    }
}

// MARK: - class Student: Workers
class Student: Workers {
    var nameStudent: String
    var surnameStudent: String
    
    init (nameStudent: String, surnameStudent: String) {
        self.nameStudent = nameStudent
        self.surnameStudent = surnameStudent
        super.init()
    }
    
    override init() {
        nameStudent = "Татьяна"
        surnameStudent = "Студентик"
        super.init()
    }
}
